package com.valueu.salesobjections;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kartiks.utility.LoggerGeneral;
import com.valueu.salesobjections.persistance.AppPreferences;
import com.valueu.salesobjections.pojos.VUser;
import com.valueu.salesobjections.pojos.VUserContacts;
import com.valueu.salesobjections.pojos.VUserContactsList;
import com.valueu.salesobjections.utility.ApiCalls;
import com.valueu.salesobjections.utility.Constants;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ActivityLogin extends ActivityBase implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    Context context=this;
    Resources resources;
    AppPreferences appPreferences;

    GoogleApiClient mGoogleApiClient;

    SignInButton plusLogin;
    EditText company;
    Button login;
    TextInputLayout companyParent;

    VUser user;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        //showCustomMessage("handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            user.setEmail(acct.getEmail());
            user.setPassword(acct.getId());
            user.setName(acct.getDisplayName());

            try{
                TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
                user.setNumber(telephonyManager.getLine1Number());
            }catch (Exception e){
                e.printStackTrace();
                user.setNumber("1234567890");
            }


            plusLogin.setVisibility(View.GONE);
            companyParent.setVisibility(View.VISIBLE);
            if (companyParent.getChildCount() == 2)
                companyParent.getChildAt(1).setVisibility(View.VISIBLE);

            login.setVisibility(View.VISIBLE);

            hideLoader();

            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            //updateUI(true);

        } else {


            hideLoader();
            showCustomMessage("Login failed" + result.getStatus().getStatusMessage() + " " + result.getStatus().getStatusCode());

            if(result.getStatus().hasResolution()){
                try {
                    showCustomMessage("starting resolution");
                    result.getStatus().startResolutionForResult(this,RC_SIGN_IN);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }


            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        resources=getResources();
        appPreferences=AppPreferences.getAppPreferences(getApplicationContext());
        user =new VUser();

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)

                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(ActivityLogin.this)
                .addOnConnectionFailedListener(ActivityLogin.this)
                .build();

        plusLogin = (SignInButton)findViewById(R.id.sign_in_button);
        plusLogin.setOnClickListener(this);
        plusLogin.setScopes(gso.getScopeArray());

        companyParent=(TextInputLayout)findViewById(R.id.CompanyParent);
        company=(EditText)findViewById(R.id.Company);
        login=(Button)findViewById(R.id.Login);

        login.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        if(v==plusLogin){
            if(isConnected()){

                showLoader();

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);


            }else{
                showCustomMessage(resources.getString(R.string.noNet));
            }
        }

        else if(v==login){

            String companyName=company.getText().toString();
            if(companyName.trim().equals("")){
                if(companyParent!=null)
                    companyParent.setError("company name is mandatory");
                else
                    LoggerGeneral.e("company parent null");
                return;
            }

            user.setCompany(companyName);
            user.setRole(Constants.roleSalesPerson);

            if(isConnected()){

                showLoader();
                ApiCalls apiCalls=Constants.getRetrofitInstance();
                Call<VUser> call= apiCalls.createUser(user);
                call.enqueue(new Callback<VUser>() {
                    @Override
                    public void onResponse(Response<VUser> response, Retrofit retrofit) {
                        hideLoader();
                        if(response.isSuccess()){

                            VUser user=response.body();
                            appPreferences.putString(resources.getString(R.string.Lname),user.getName());
                            appPreferences.putString(resources.getString(R.string.Lpassword),user.getPassword());
                            appPreferences.putString(resources.getString(R.string.Lrole),user.getRole());
                            appPreferences.putString(resources.getString(R.string.LsessionId),user.getSessionId());
                            appPreferences.putString(resources.getString(R.string.Lcompany),user.getCompany());
                            appPreferences.putString(resources.getString(R.string.Lnumber), user.getNumber());
                            appPreferences.putString(resources.getString(R.string.Lemail), user.getEmail());
                            appPreferences.putLong(resources.getString(R.string.Lid), user.getId());

                            if(mayRequestContacts())
                                syncContacts();

                        }else{

                            handleFailure(response);
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                        hideLoader();
                        LoggerGeneral.e("error " + t.toString());
                    }
                });

            }else
                showCustomMessage(resources.getString(R.string.noNet));

        }
    }

    /*
    *
    * When the user has successfully signed in,
    * your onConnected handler will be called.
    * At this point, you are able to retrieve the user’s account name
    * or make authenticated requests.
    * For example, once your GoogleApiClient is connected you can use the
    * Plus.PeopleApi.getCurrentPerson method to retrieve information about the signed-in user
    *
    * */
    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        //showToast("onConnected:" + bundle);

        // Show the signed-in UI
        //showSignedInUI();
//        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
//            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
//
//            String personName = currentPerson.getDisplayName();
//            String personPhoto = currentPerson.getImage().getUrl();
//            String personGooglePlusProfile = currentPerson.getUrl();
//
//            showCustomMessage(email+personName+currentPerson.getId());
//            hideLoader();
//            //Intent i =new Intent(context,ActivityRootMaterial.class);
//            //startActivity(i);
//
//        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        showCustomMessage("onConnectionFailed:" + connectionResult);


            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, RC_SIGN_IN);

                } catch (IntentSender.SendIntentException e) {
                    showCustomMessage("Could not resolve ConnectionResult." + e);


                    mGoogleApiClient.connect();
                }
            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
                showCustomMessage(connectionResult.getErrorCode() + " " + connectionResult.getErrorMessage());
            }
    }

    public void syncContacts(){
        showLoader();
        VUserContactsList toSend=fetchContacts();
        ApiCalls apiCalls=Constants.getRetrofitInstance();
        Call<Void> call= apiCalls.createBulkContacts(getAuthorizationString(resources,appPreferences),toSend);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response, Retrofit retrofit) {
                hideLoader();
                if (response.isSuccess()) {

                    showToast("Success");
                    Intent i = new Intent(context, ActivityPieChart.class);
                    startActivity(i);
                    finish();
                } else {

                    handleFailure(response);
                }
            }

            @Override
            public void onFailure(Throwable t) {

                hideLoader();
                LoggerGeneral.e("error" + t.toString());
            }
        });

    }

    public VUserContactsList fetchContacts() {

        VUserContactsList returnList=new VUserContactsList();
        List<VUserContacts> vUserContactsList=returnList.getList();

        //String phoneNumber = null;
        //String email = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;

        //StringBuffer output = new StringBuffer();

        ContentResolver contentResolver = getContentResolver();

        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        String nameColumn=null;
        if(Build.VERSION.SDK_INT
                >= Build.VERSION_CODES.HONEYCOMB){
            nameColumn=ContactsContract.Contacts.DISPLAY_NAME_PRIMARY;
        }else
            nameColumn=ContactsContract.Contacts.DISPLAY_NAME;


        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {


                String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
                String name = cursor.getString(cursor.getColumnIndex( nameColumn ));

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));

                if (hasPhoneNumber > 0) {

                    VUserContacts userContacts=new VUserContacts();
                    userContacts.setUserId(appPreferences.getLong(resources.getString(R.string.Lid),1));
                    userContacts.setContactName(name);

                    // Query and loop for every phone number of the contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);

                    String phoneNumber="";
                    while (phoneCursor.moveToNext()) {

                        phoneNumber+=  phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER))+" ";
                        userContacts.setContactNo(phoneNumber);
                    }

                    phoneCursor.close();

                    // Query and loop for every email of the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI,	null, EmailCONTACT_ID+ " = ?", new String[] { contact_id }, null);
                    String email="";
                    while (emailCursor.moveToNext()) {

                        email += emailCursor.getString(emailCursor.getColumnIndex(DATA))+" ";
                        userContacts.setEmail(email);

                    }

                    emailCursor.close();
                    vUserContactsList.add(userContacts);
                }
            }
        }

        return returnList;
    }

    private boolean mayRequestContacts() {

        final String READ_CONTACTS= Manifest.permission.READ_CONTACTS;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(login, "Allow applications to read contacts", Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if(requestCode==REQUEST_READ_CONTACTS){
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                syncContacts();

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                showCustomMessage("Contacts are required for the app to continue");
            }
        }

    }

}

