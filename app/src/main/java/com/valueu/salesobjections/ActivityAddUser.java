package com.valueu.salesobjections;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kartiks.utility.LoggerGeneral;
import com.valueu.salesobjections.persistance.AppPreferences;
import com.valueu.salesobjections.pojos.VUser;
import com.valueu.salesobjections.utility.ApiCalls;
import com.valueu.salesobjections.utility.Constants;
import com.valueu.salesobjections.utility.StringUtil;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ActivityAddUser extends ActivityBase implements View.OnClickListener{

    Context context=this;
    Resources resources;
    AppPreferences appPreferences;

    EditText email;
    Button addUser;
    TextInputLayout emailParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        resources=getResources();
        appPreferences=AppPreferences.getAppPreferences(context);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        email=(EditText)findViewById(R.id.Email);
        emailParent=(TextInputLayout)findViewById(R.id.EmailParent);
        addUser=(Button)findViewById(R.id.Login);
        addUser.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if(v==addUser){

            String emailx=email.getText().toString();
            if(emailx.trim().length()==0){
                emailParent.setError("Enter email");
                return;
            }
            if(!StringUtil.isValidEmailAddress(emailx)){
                emailParent.setError("Enter valid email");
                return;
            }

            VUser user=new VUser();
            user.setRole(Constants.roleSalesPerson);
            user.setCreatedBy(appPreferences.getLong(resources.getString(R.string.Lid), -1));
            user.setPassword(emailx + "temp");
            user.setNumber("1234567890");
            user.setCompany(appPreferences.getString(resources.getString(R.string.Lcompany), ""));
            user.setName("temp");
            user.setEmail(emailx);

            if(isConnected()){

                ApiCalls apiCalls=Constants.getRetrofitInstance();
                Call<VUser>call= apiCalls.createUser(user);
                call.enqueue(new Callback<VUser>() {
                    @Override
                    public void onResponse(Response<VUser> response, Retrofit retrofit) {
                        hideLoader();
                        if(response.isSuccess()){

                            showToast("Success");
                            finish();

                        }else{

                            handleFailure(response);
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                        hideLoader();
                        LoggerGeneral.e("error " + t.toString());
                    }
                });

            }else
                showCustomMessage(resources.getString(R.string.noNet));

        }
    }
}
