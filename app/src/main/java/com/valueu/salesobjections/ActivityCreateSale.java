package com.valueu.salesobjections;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.kartiks.utility.LoggerGeneral;
import com.valueu.salesobjections.persistance.AppPreferences;
import com.valueu.salesobjections.persistance.DBHelper;
import com.valueu.salesobjections.pojos.PredefinedReason;
import com.valueu.salesobjections.pojos.VSales;
import com.valueu.salesobjections.utility.ApiCalls;
import com.valueu.salesobjections.utility.Constants;
import com.valueu.salesobjections.utility.StringUtil;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ActivityCreateSale extends ActivityBase implements View.OnClickListener {

    Context context=this;
    Resources resources;
    AppPreferences appPreferences;

    CheckBox saleMade;
    Spinner reason;
    LinearLayout saleNotMadeParent;
    EditText description,otherReason,customerName,customerNumber,customerEmail;
    TextInputLayout otherReasonWrapper,descriptionWrapper,customerNameWrapper,customerNumberWrapper,customerEmailWrapper;

    Button submit;

    DBHelper dbHelper;
    ArrayAdapter<String> reasonAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_sale);

        resources=getResources();
        appPreferences=AppPreferences.getAppPreferences(getApplicationContext());
        dbHelper=DBHelper.getInstance(context);

        saleMade=(CheckBox)findViewById(R.id.SaleMade);
        reason=(Spinner)findViewById(R.id.ReasonSpinner);
        description=(EditText)findViewById(R.id.Description);
        saleNotMadeParent=(LinearLayout)findViewById(R.id.SaleNotMadeParent);
        submit=(Button)findViewById(R.id.Submit);

        customerEmail=(EditText)findViewById(R.id.CustomerEmail);
        customerName=(EditText)findViewById(R.id.CustomerName);
        customerNumber=(EditText)findViewById(R.id.CustomerNumber);
        otherReason=(EditText)findViewById(R.id.OtherReason);
        description=(EditText)findViewById(R.id.Description);

        otherReasonWrapper=(TextInputLayout)findViewById(R.id.OtherReasonWrapper);
        descriptionWrapper=(TextInputLayout)findViewById(R.id.DescriptionWrapper);
        customerNameWrapper=(TextInputLayout)findViewById(R.id.CustomerNameWrapper);
        customerNumberWrapper=(TextInputLayout)findViewById(R.id.CustomerNumberWrapper);
        customerEmailWrapper=(TextInputLayout)findViewById(R.id.CustomerEmailWrapper);

        submit.setOnClickListener(this);

        saleMade.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                    saleNotMadeParent.setVisibility(View.GONE);
                else
                    saleNotMadeParent.setVisibility(View.VISIBLE);
            }
        });

        final ArrayList<PredefinedReason> predefinedReasonArrayList=new ArrayList<PredefinedReason>();
        predefinedReasonArrayList.add(new PredefinedReason("Select"));

        predefinedReasonArrayList.addAll((ArrayList<PredefinedReason>) dbHelper.genericRead(PredefinedReason.class,
                DBHelper.reasonsTable, null, null, null, null, null));

        predefinedReasonArrayList.add(new PredefinedReason("Other"));

        reasonAdapter=new ArrayAdapter
                (context,android.R.layout.simple_list_item_1, android.R.id.text1, predefinedReasonArrayList);

        reason.setAdapter(reasonAdapter);
        reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PredefinedReason pr = predefinedReasonArrayList.get(position);
                if (pr.getName().equals("Other")) {
                    otherReasonWrapper.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        if(v==submit) {

            VSales vSale = new VSales();
            vSale.setSalesPersonId(appPreferences.getLong(resources.getString(R.string.Lid),1));
            vSale.setSaleMade(saleMade.isChecked());
            if (!saleMade.isChecked()) {
                PredefinedReason pr = (PredefinedReason) reason.getSelectedItem();
                if (pr.getName().equals("Select")) {

                    showCustomMessage("Select a reason");
                    return;
                }
                if (pr.getName().equals("Other")) {

                    String otherReasonText = otherReason.getText().toString();
                    if (otherReasonText.trim().equals("")) {
                        otherReasonWrapper.setError("Enter reason for sale not made");
                        return;
                    }

                    String[] whereArgs={otherReasonText};
                    PredefinedReason predefinedReason=(PredefinedReason)dbHelper.genericRead(PredefinedReason.class, DBHelper.reasonsTable, DBHelper.reasonName + "=?", whereArgs, null, null, null);
                    if(predefinedReason==null){
                        //add in db
                        predefinedReason=new PredefinedReason(otherReasonText);
                        dbHelper.insertGeneric(predefinedReason,DBHelper.reasonsTable);
                    }else{
                        showCustomMessage(otherReasonText +" already exists in database, select the same");
                        return ;
                    }

                    vSale.setLostReason(otherReasonText);
                    vSale.setDescription(description.getText().toString());

                } else {
                    vSale.setLostReason(pr.getName());
                    vSale.setDescription(description.getText().toString());
                }

                String customerNamex=customerName.getText().toString();
                String customerNumberx=customerNumber.getText().toString();
                if(customerNamex.trim().isEmpty()){
                    customerNameWrapper.setError("customer name is required");
                    return;
                }
                if(customerNumberx.trim().isEmpty()){
                    customerNumberWrapper.setError("customer number is required");
                    return;
                }
                LoggerGeneral.e("length "+customerNumberx.trim().length());
                if(!(customerNumberx.trim().length()==10 )){
                    customerNumberWrapper.setError("customer number can be 10 or 12 characters");
                    return;
                }

                vSale.setCustomerName(customerNamex);
                vSale.setCustomerMobile(customerNumberx);

                String email=customerEmail.getText().toString();
                if(email.trim().length()>0 && !StringUtil.isValidEmailAddress(email)){
                    customerEmailWrapper.setError("email not valid");
                    return;
                }
                vSale.setCustomerEmail(customerEmail.getText().toString());

            }

            //make netwoerk request and finish();
            if(isConnected()){

                showLoader();
                ApiCalls apiCalls= Constants.getRetrofitInstance();
                Call<VSales> call= apiCalls.createSale(getAuthorizationString(resources,appPreferences),vSale);
                call.enqueue(new Callback<VSales>() {
                    @Override
                    public void onResponse(Response<VSales> response, Retrofit retrofit) {
                        hideLoader();
                        if(response.isSuccess()){

                            showToast("Success");
                            finish();
                        }else
                            handleFailure(response);

                    }

                    @Override
                    public void onFailure(Throwable t) {

                        hideLoader();
                        LoggerGeneral.e("here " + t.toString());
                        showCustomMessage("here " + t.getMessage());
                    }
                });

            }else
                showCustomMessage(resources.getString(R.string.noNet));

        }
    }
}
