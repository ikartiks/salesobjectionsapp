package com.valueu.salesobjections.pojos;

public class VUserContacts extends VDataObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3970078304880889664L;
	
	Long userId;
	
	String contactNo;

    String contactName;

    String email;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
