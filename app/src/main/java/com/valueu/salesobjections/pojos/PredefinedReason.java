package com.valueu.salesobjections.pojos;

/**
 * Created by 1078943 on 12/31/2015.
 */
public class PredefinedReason {

    public PredefinedReason() {

    }

    public PredefinedReason(String name) {
        this.name = name;
    }

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
