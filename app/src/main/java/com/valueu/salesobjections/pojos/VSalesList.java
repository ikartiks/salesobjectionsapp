package com.valueu.salesobjections.pojos;

import java.util.ArrayList;
import java.util.List;

public class VSalesList extends VList {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7943261144349658462L;
	
	List<VSales> list=new ArrayList<VSales>();
	
	@Override
	public int getListSize() {
		
		return list.size();
	}

	@Override
	public List<VSales> getList() {
		
		return list;
	}

}
