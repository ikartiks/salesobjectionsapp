package com.valueu.salesobjections.pojos;

import java.util.ArrayList;
import java.util.List;

public class VUserContactsList extends VList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7955141537504350069L;
	
	List<VUserContacts> list=new ArrayList<VUserContacts>();
	
	@Override
	public int getListSize() {
		return list.size();
	}

	@Override
	public List<VUserContacts> getList() {
		return list;
	}
}
