package com.valueu.salesobjections.pojos;

public class VSales extends VDataObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7475438694863487153L;
	
	Long salesPersonId;
	
	Boolean saleMade;
	
	String lostReason;
	
	String description;
	
	String customerName;
	
	String customerMobile;
	
	String customerEmail;
	
	Long countOfOccurenes;
	

	public Long getSalesPersonId() {
		return salesPersonId;
	}

	public void setSalesPersonId(Long salesPersonId) {
		this.salesPersonId = salesPersonId;
	}

	public Boolean getSaleMade() {
		return saleMade;
	}

	public void setSaleMade(Boolean saleMade) {
		this.saleMade = saleMade;
	}

	public String getLostReason() {
		return lostReason;
	}

	public void setLostReason(String lostReason) {
		this.lostReason = lostReason;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public Long getCountOfOccurenes() {
		return countOfOccurenes;
	}

	public void setCountOfOccurenes(Long countOfOccurenes) {
		this.countOfOccurenes = countOfOccurenes;
	}
	
}
