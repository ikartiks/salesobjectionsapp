package com.valueu.salesobjections.utility;


import com.valueu.salesobjections.pojos.VSales;
import com.valueu.salesobjections.pojos.VSalesList;
import com.valueu.salesobjections.pojos.VUser;
import com.valueu.salesobjections.pojos.VUserContactsList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by kartikshah on 25/12/15.
 */
public interface ApiCalls {

    @POST("user/user_mobile")
    Call<VUser> createUser(@Body VUser user);

    @POST("basic/bulk_contacts_create")
    Call<Void> createBulkContacts(@Header("Authorization") String authorization,@Body VUserContactsList vSales);

    @POST("basic/sales")
    Call<VSales> createSale(@Header("Authorization") String authorization,@Body VSales vSales);

    @GET("basic/salesCount/{id}/{all}")
    Call<VSalesList> fetchSales(@Header("Authorization") String authorization,@Path("id") Long id,@Path("all") Boolean all);

    @GET("basic/salesBreakup/")
    Call<VSalesList> getPersonWiseBreakup(@Header("Authorization") String authorization);


}
