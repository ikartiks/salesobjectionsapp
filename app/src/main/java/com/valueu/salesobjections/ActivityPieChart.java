package com.valueu.salesobjections;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.kartiks.utility.LoggerGeneral;
import com.valueu.salesobjections.adapters.RecyclerAdapter;
import com.valueu.salesobjections.persistance.AppPreferences;
import com.valueu.salesobjections.pojos.VSales;
import com.valueu.salesobjections.pojos.VSalesList;
import com.valueu.salesobjections.utility.ApiCalls;
import com.valueu.salesobjections.utility.Constants;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ActivityPieChart extends ActivityBase {


    Context context=this;
    Resources resources;
    AppPreferences appPreferences;

    PieChart mPieChart;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<VSales> salesArrayList;

    int[] colors= {Color.parseColor("#990F0F"),Color.parseColor("#082AC4") ,Color.parseColor("#086925")
            ,Color.parseColor("#0040ff"),Color.parseColor("#180B8A"),Color.parseColor("#00cc44") ,Color.parseColor("#ff1a1a"),
            Color.parseColor("#0040ff"),Color.parseColor("#8A0B77") ,Color.parseColor("#0898A8"),
            Color.parseColor("#A119CF") ,Color.parseColor("#CF19BF"),Color.parseColor("#ED6BA5"),
            Color.parseColor("#3B9617") ,Color.parseColor("#D1F062"),
            Color.parseColor("#5047A1") ,Color.parseColor("#E09010"),Color.parseColor("#2510E0"),Color.parseColor("#C4271B")
            ,Color.parseColor("#ff99ff"),Color.parseColor("#ffad33") ,Color.parseColor("#ff704d"),Color.parseColor("#ff3399")
            ,Color.parseColor("#944dff") ,Color.parseColor("#ffff00")};

    @Override
    protected void onResume() {
        super.onResume();


        if(isConnected()){

            showLoader();

            Boolean all=false;
            long id =appPreferences.getLong(resources.getString(R.string.LUserPieChartId),-1);
            if(id==-1){
                id=appPreferences.getLong(resources.getString(R.string.Lid), 1);
                all=true;
            }

            fetchSalesOfPerson(id,all);

            ApiCalls apiCalls= Constants.getRetrofitInstance();

            if(!appPreferences.getString(resources.getString(R.string.Lrole),"").equals(Constants.roleSalesPerson)){

                Call<VSalesList> personWiseBreakup= apiCalls.getPersonWiseBreakup(getAuthorizationString(resources, appPreferences));
                personWiseBreakup.enqueue(new Callback<VSalesList>() {
                    @Override
                    public void onResponse(Response<VSalesList> response, Retrofit retrofit) {

                        hideLoader();
                        if (response.isSuccess()) {

                            VSalesList vSalesList = response.body();
                            List<VSales> list = vSalesList.getList();
                            salesArrayList.clear();
                            salesArrayList.addAll(list);
                            recyclerAdapter.notifyDataSetChanged();

                        } else
                            handleFailure(response);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        hideLoader();
                        LoggerGeneral.e("failed " + t.getMessage());
                    }
                });
            }

        }else
            showCustomMessage(resources.getString(R.string.noNet));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pie_chart);
        resources=getResources();
        appPreferences=AppPreferences.getAppPreferences(context);

        appPreferences.putLong(resources.getString(R.string.LUserPieChartId), -1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(appPreferences.getString(resources.getString(R.string.Lrole),"").equals(Constants.roleSalesPerson))
            getSupportActionBar().setTitle(appPreferences.getString(resources.getString(R.string.Lname), ""));
        else
            getSupportActionBar().setTitle(appPreferences.getString(resources.getString(R.string.Lname), "")+" consolidated");

        mPieChart = (PieChart) findViewById(R.id.piechart);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                Intent i =new Intent(context,ActivityCreateSale.class);
                startActivity(i);
            }
        });

        recyclerView=(RecyclerView)findViewById(R.id.RecyclerView);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        salesArrayList =new ArrayList<VSales>();

        recyclerAdapter=new RecyclerAdapter(salesArrayList);
        recyclerView.setAdapter(recyclerAdapter);

        recyclerAdapter.setOnItemClickLickListener(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                VSales currentPerson=salesArrayList.get(position);
                appPreferences.putLong(resources.getString(R.string.LUserPieChartId), currentPerson.getSalesPersonId());
                Boolean all=false;
                if(currentPerson.getCountOfOccurenes()==-1)
                    all=true;
                fetchSalesOfPerson(currentPerson.getSalesPersonId(),all);
                getSupportActionBar().setTitle(currentPerson.getCustomerName());
            }
        });

    }

    public void fetchSalesOfPerson(Long personId,boolean all){

        mPieChart.clearChart();
        ApiCalls apiCalls= Constants.getRetrofitInstance();
        Call<VSalesList> call= apiCalls.fetchSales(getAuthorizationString(resources,appPreferences),personId,all);
        call.enqueue(new Callback<VSalesList>() {
            @Override
            public void onResponse(Response<VSalesList> response, Retrofit retrofit) {

                hideLoader();
                if (response.isSuccess()) {

                    VSalesList vSalesList = response.body();
                    List<VSales> list = vSalesList.getList();
                    VSales sale;
                    int listSize = list.size();
                    for (int i = 0; i < listSize; i++) {
                        sale = list.get(i);
                        mPieChart.addPieSlice(new PieModel(sale.getLostReason(),
                                sale.getCountOfOccurenes(), colors[i% (colors.length - 1)]));
                    }
                    if (listSize > 0)
                        mPieChart.startAnimation();

                } else
                    handleFailure(response);
            }

            @Override
            public void onFailure(Throwable t) {
                hideLoader();
                LoggerGeneral.e("failed " + t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.menu_activity_pie_chart, menu);
        if(appPreferences.getString(resources.getString(R.string.Lrole),"").equals(Constants.roleSalesManager))
            return true;
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.AddUser) {
            Intent intent=new Intent(context,ActivityAddUser.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
