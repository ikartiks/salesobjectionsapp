package com.valueu.salesobjections;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import com.valueu.salesobjections.persistance.AppPreferences;

public class ActivitySplash extends ActivityBase {

    Context context=this;
    Resources resources;
    AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        resources=getResources();
        appPreferences=AppPreferences.getAppPreferences(context);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {

                    Intent intent=null;
                    if(appPreferences.getLong(resources.getString(R.string.Lid),1)==1){
                        intent=new Intent(context,ActivityLogin.class);
                    }else{
                        intent=new Intent(context,ActivityPieChart.class);
                    }

                    startActivity(intent);
                    finish();
                }
            }
        }).start();
    }
}
